<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('register');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request['Fname'];
        $namaBelakang = $request['Lname'];
        return view('Welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }

    public function Welcome(){
        return view('welcome');
    }
}
