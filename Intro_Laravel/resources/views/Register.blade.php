<!DOCTYPE html>
<html>
<head>
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        
        <label>First Name:</label><br><br>
        <input type="text" name="Fname"><br><br>
    
        <label>Last Name:</label><br><br>
        <input type="text" name="Lname"><br><br>
    
        <label>Gender:</label><br><br>
        <input type="radio" name="Mgender">Male<br>
        <input type="radio" name="Fgender">Female<br>
        <input type="radio" name="Ogender">Other<br><br>
    
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
    
        <label>Language Spoken:</label><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other<br><br>
    
        <label>Bio:</label><br><br>
        <textarea name="message" cols="30" rows="10"></textarea><br>
        <button>Sign Up</button>
    </form>

</body>